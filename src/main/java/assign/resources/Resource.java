package assign.resources;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import assign.services.Meetings;
import assign.services.Project;
import assign.services.Source;

@Path("/projects")
public class Resource {
	
	Source src = new Source();
	
	@GET
	@Path("/{project}/meetings/{year}")
	@Produces("application/xml")
	public Response printMeetingYear(@PathParam("project") String project, @PathParam("year") String year) throws IOException {
		String result = src.getProjectYear(project, year);
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/{project}/irclogs")
	@Produces("application/xml")
	public Response printIrclogs(@PathParam("project") String project) throws IOException {
		String result = src.getProject(project);
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/")
	@Produces("application/xml")
	public Response printUnion() throws IOException {
		String result = src.getUnion(); 
		return Response.status(200).entity(result).build();
	}
	
	@POST
	@Path("/")
	@Consumes("application/xml")
	public Response createProject(Project project) {
		boolean result = src.createProject(project.getName(), project.getDesc());
		return Response.status(result ? 200 : 400).build();
	}
	
	@PUT
	@Path("/{project}")
	@Consumes("application/xml")
	public Response updateProject(@PathParam("project") String project, Project proj) {
		//need to get the required data from the body 
		boolean result = src.updateProject(project, proj.getDesc());
		return Response.status(result ? 200 : 400).build();
	}
	
	@POST
	@Path("/{project}/meetings")
	@Consumes("application/xml")
	public Response createMeeting(@PathParam("project") String project, Meetings meeting) {
		//need to get the required data from the body 
		boolean result = src.createMeeting(project, meeting.getName(), meeting.getLink(), meeting.getYear());
		return Response.status(result ? 200 : 400).build();
	}
	
	@GET
	@Path("/{project}")
	@Produces("application/xml")
	public Response getProject(@PathParam("project") String project) {
		String result = src.getProjectFromDB(project);
		return Response.status(200).entity(result).build();
	}
	
	@DELETE
	@Path("/{project}")
	public Response deleteProject(@PathParam("project") String project) {
		boolean result = src.deleteProject(project);
		return Response.status(result ? 200 : 400).build();
	}
}
