package assign.services;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;


public class Source {

	private static final Logger LOGGER = Logger.getLogger(Source.class.getName());
	private String url = "http://eavesdrop.openstack.org/"; 	

	// JDBC driver name and database URL
	static final String DB_URL = "jdbc:mysql://localhost/assignmentfive";

	//  Database credentials
	static final String USER = "root";
	static final String PASS = "1234";

	public String getProjectYear(String project, String year) throws IOException{

		String url = this.url;
		url += "meetings/" + project + "/" + year;		
		Document doc = Jsoup.connect(url).get();

		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append(" <project name=\'" + project + "'>");

		Elements links = doc.select("a[href*=" + project + "." + year + "]");
		for(Element link: links){
			xml.append(" <link>" + link.attr("abs:href") + "</link>");
		}

		xml.append(" </project>");

		return xml.toString();		
	}

	public String getProject(String project) throws IOException{
		String url = this.url;
		url += "irclogs" + "/%23" + project.substring(1, project.length());		
		Document doc = Jsoup.connect(url).get();

		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append("<project name='" + project.substring(1, project.length()) + "'>");

		Elements links = doc.select("a[href*=%23]");
		for(Element link: links){
			xml.append("<link>" + link.attr("abs:href") + "</link>");
		}

		xml.append("</project>");

		return xml.toString(); 
	}

	public String getUnion() throws IOException{
		String url = this.url;
		url += "irclogs";
		Document doc = Jsoup.connect(url).get();

		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append("<projects>");

		Elements links = doc.select("a[href*=%23]");
		for(Element link: links){
			xml.append("<link>" + link.attr("abs:href") + "</link>");
		}

		url = this.url;
		url += "meetings";
		int i = 0;

		Document doc2 = Jsoup.connect(url).get();

		Elements correct_doc = doc2.select("a[href]");
		for(Element correct_link: correct_doc){
			i++; correct_link.attr("abs:href");
			if(i > 5){
				xml.append("<link>" + correct_link.attr("abs:href") + "</link>");
			}
		}

		xml.append("</projects>");

		return xml.toString();
	}

	public boolean createProject(String project, String description) {
		Connection conn = null;
		Statement stmt = null;
		int success = 0;
		PreparedStatement updateProject = null;

		try{		
			Class.forName("com.mysql.jdbc.Driver");

			LOGGER.info("Logger Name: "+LOGGER.getName());
			LOGGER.info("Connecting to database...");

			conn = DriverManager.getConnection(DB_URL,USER,PASS);

			LOGGER.info("Creating statement..."); 
			conn.setAutoCommit(false);
			String sqlproject = "INSERT INTO project VALUES (\'" + project + "\', \'" + description + "\');";

			updateProject = conn.prepareStatement(sqlproject);
			success = updateProject.executeUpdate();

			conn.commit();
			conn.close();
		}catch(SQLException se){
			LOGGER.info("Handle errors for JDBC");
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			LOGGER.info("finally block used to close resources");
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}LOGGER.warning("nothing we can do");
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}LOGGER.info("end finally try");
		}LOGGER.info("end try");
		return success > 0 ? true : false;
	}

	public boolean updateProject(String project, String description) {
		Connection conn = null;
		Statement stmt = null;
		int success = 0;
		PreparedStatement updateProject = null;

		try{	
			Class.forName("com.mysql.jdbc.Driver");

			LOGGER.info("Logger Name: "+LOGGER.getName());
			LOGGER.info("Connecting to database...");

			conn = DriverManager.getConnection(DB_URL,USER,PASS);

			LOGGER.info("Creating statement..."); 
			conn.setAutoCommit(false);
			String sqlproject = "UPDATE project SET description= \'" + description + "\' WHERE name= \'" + project + "\';";

			updateProject = conn.prepareStatement(sqlproject);
			success = updateProject.executeUpdate();

			conn.commit();
			conn.close();
		}catch(SQLException se){
			LOGGER.info("Handle errors for JDBC");
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			LOGGER.info("finally block used to close resources");
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}LOGGER.warning("nothing we can do");
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}LOGGER.info("end finally try");
		}LOGGER.info("end try");
		return success > 0 ? true : false;
	}

	public boolean createMeeting(String project, String name, String link, String year) {
		Connection conn = null;
		Statement stmt = null;
		int success = 0;
		PreparedStatement updateMeetings = null;

		try{		
			Class.forName("com.mysql.jdbc.Driver");

			LOGGER.info("Logger Name: "+LOGGER.getName());
			LOGGER.info("Connecting to database...");

			conn = DriverManager.getConnection(DB_URL,USER,PASS);

			LOGGER.info("Creating statement..."); 
			conn.setAutoCommit(false);
			String sqlproject = "INSERT INTO meetings VALUES (\'" + project + "\', \'" + name + "\', \'" + link + "\', \'" + year + "\');";

			updateMeetings = conn.prepareStatement(sqlproject);
			success = updateMeetings.executeUpdate();

			conn.commit();
			conn.close();
		}catch(SQLException se){
			LOGGER.info("Handle errors for JDBC");
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			LOGGER.info("finally block used to close resources");
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}LOGGER.warning("nothing we can do");
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}LOGGER.info("end finally try");
		}LOGGER.info("end try");
		return success > 0 ? true : false;
	}

	public String getProjectFromDB(String project) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement updateProject = null;
		PreparedStatement updateMeetings = null;
		StringBuffer xml = new StringBuffer();

		try{		
			Class.forName("com.mysql.jdbc.Driver");
			LOGGER.info("Logger Name: "+LOGGER.getName());
			LOGGER.info("Connecting to database...");

			conn = DriverManager.getConnection(DB_URL,USER,PASS);

			LOGGER.info("Creating statement..."); 
			conn.setAutoCommit(false);

			String sqlmeetings = "SELECT name, link, year FROM meetings WHERE idmeetings = \'" + project + "\'";
			String sqlproject = "SELECT name, description FROM project WHERE name = \'" + project + "\'";

			updateProject = conn.prepareStatement(sqlproject);
			updateMeetings = conn.prepareStatement(sqlmeetings);

			ResultSet res = updateProject.executeQuery();
			ResultSet result = updateMeetings.executeQuery();

			//Retrieve by column name
			String mname = "";
			String link = "";
			String year = "";
			while(result.next()){
				mname  = result.getString("name");
				link = result.getString("link");
				year = result.getString("year");
			}

			String pname = "";
			String desc = "";
			while(res.next()){
				pname = res.getString("name");
				desc = res.getString("description");
			}
			
			xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xml.append("<project>");
			xml.append("<name> " + pname + " </name>");
			xml.append("<description> " + desc + " </description>");
			xml.append("<meetings>");
			xml.append("<meeting>");
			xml.append("<name> " + mname + " </name>");
			xml.append("<link> " + link + " </link>");
			xml.append("<year> " + year + " </year>");
			xml.append("</meeting>");
			xml.append("</meetings>");
			xml.append("</project>");

			conn.close();
		}catch(SQLException se){
			LOGGER.info("Handle errors for JDBC");
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			LOGGER.info("finally block used to close resources");
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}LOGGER.warning("nothing we can do");
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}LOGGER.info("end finally try");
		}LOGGER.info("end try");

		return xml.toString();
	}

	public boolean deleteProject(String project) {
		Connection conn = null;
		Statement stmt = null;
		int success = 0;
		PreparedStatement updateProject = null;
		PreparedStatement updateMeetings = null;

		try{		
			Class.forName("com.mysql.jdbc.Driver");
			LOGGER.info("Logger Name: "+LOGGER.getName());
			LOGGER.info("Connecting to database...");

			conn = DriverManager.getConnection(DB_URL,USER,PASS);

			LOGGER.info("Creating statement..."); 
			conn.setAutoCommit(false);
			String sqlproject = "DELETE FROM project WHERE name = \'" + project + "\';";
			String sqlmeetings = "DELETE FROM meetings WHERE idmeetings = \'" + project + "\';";

			updateProject = conn.prepareStatement(sqlproject);
			updateMeetings = conn.prepareStatement(sqlmeetings);

			success = updateProject.executeUpdate();
			success = updateMeetings.executeUpdate();

			conn.commit();
			conn.close();
		}catch(SQLException se){
			LOGGER.info("Handle errors for JDBC");
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			LOGGER.info("finally block used to close resources");
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}LOGGER.warning("nothing we can do");
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}LOGGER.info("end finally try");
		}LOGGER.info("end try");

		return success > 0 ? true : false;
	}
}
